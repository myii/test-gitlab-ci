#!/bin/sh

###############################################################################
# (A) Update app version with `${nextRelease.version}`
###############################################################################
sed -i -e "s@^\(set(VERSION\s\+\"\).*\(\")\)\$@\1${1}\2@" CMakeLists.txt
sed -i -e "s@^\(\s\+readonly property var appVersion: \"\).*\(\"\)\$@\1${1}\2@" src/app/qml/main.qml
