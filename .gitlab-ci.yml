---
###############################################################################
# Define all YAML node anchors
###############################################################################
.node_anchors:
  # `stage`
  stage_lint: &stage_lint 'lint'
  stage_release: &stage_release 'release'
  stage_build: &stage_build 'build'
  # `image`
  image_node: &image_node 'node:12-buster-slim'
  image_python: &image_python 'python:3.8-alpine'
  image_shellcheck: &image_shellcheck 'koalaman/shellcheck-alpine:latest'
  # `before_script`
  before_script_image_node: &before_script_image_node
    - 'apt-get update
       && apt-get install -y --no-install-recommends git-core ca-certificates'
  before_script_build_click: &before_script_build_click
    - |
      if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
        git checkout master
      else
        echo "Append date/time to version number here..."
        VERSION=$(grep "set(VERSION" CMakeLists.txt | cut -d\" -f2)
        VERSION=${VERSION}_$(date "+%Y-%m-%d_%H-%M-%S")
        echo "${VERSION}"
        sh ./pre-commit_semantic-release.sh ${VERSION}
      fi
  # `only` (also used for `except` where applicable)
  only_branch_master: &only_branch_master ['master']

###############################################################################
# Define `build` template
###############################################################################
.build_click:
  stage: *stage_build
  before_script: *before_script_build_click
  script:
    - 'grep "set(VERSION" CMakeLists.txt'
    - 'grep "readonly property var appVersion" src/app/qml/main.qml'

###############################################################################
# Define stages
###############################################################################
stages:
  - 'lint'
  - 'release'
  - 'build'

###############################################################################
# `lint` stage: `yamllint`, `shellcheck` & `commitlint`
###############################################################################
yamllint:
  stage: *stage_lint
  image: *image_python
  script:
    # # Install and run `salt-lint`
    # - 'pip install --user salt-lint'
    # - "git ls-files -- '*.sls' '*.jinja' '*.j2' '*.tmpl' '*.tst'
    #                  | xargs salt-lint"
    # Install and run `yamllint`
    # Need at least `v1.17.0` for the `yaml-files` setting
    # Following results in `/root/.local/bin` being used (not in `PATH`):
    # - 'pip install --user yamllint>=1.17.0'
    - 'pip install yamllint>=1.17.0'
    - 'yamllint -s .'
    # # Install and run `rubocop`
    # - 'gem install rubocop'
    # - 'rubocop -d'

shellcheck:
  stage: *stage_lint
  image: *image_shellcheck
  script:
    - 'shellcheck --version'
    - 'find . -type f \( -iname \*.sh -o -iname \*.bash -o -iname \*.ksh \)
                      | xargs shellcheck'

commitlint:
  stage: *stage_lint
  image: *image_node
  before_script: *before_script_image_node
  script:
    # Install and run `commitlint`
    # - 'export'
    - 'git fetch --all'
    - 'npm install -D @commitlint/config-conventional
                      @commitlint/cli'
    # `${CI_COMMIT_BEFORE_SHA}` only gives the previous commit
    # - 'npx commitlint --from "${CI_COMMIT_BEFORE_SHA}"'
    - 'npx commitlint --from "$(git merge-base origin/master HEAD)"
                      --to   "${CI_COMMIT_SHA}"'

###############################################################################
# `release` stage: `semantic-release`
###############################################################################
semantic-release:
  only: *only_branch_master
  stage: *stage_release
  image: *image_node
  before_script: *before_script_image_node
  script:
    - 'npm install -g semantic-release
                      @semantic-release/gitlab
                      @semantic-release/changelog
                      @semantic-release/exec
                      @semantic-release/git'
    - 'semantic-release'

###############################################################################
# `build` stage: `build_click_xenial_armhf` (simulated)
###############################################################################
# Using terse format in preparation for SaltStack Kitchen testing layout
build_click_xenial_armhf: {extends: '.build_click'}
